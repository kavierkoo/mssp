#MSSP Assignment
This is a system developed by Kavier Koo (for Python) and Tan Li Teen (for Ruby) as assignment for module named Malicious Software and Security Programming with the objective to 
develop a bot that could take control of a system. 

The system automated MSFVenom by connecting to the internet with a local IP address (connected to network) and 
send spoof mail to "victims" to ask them to click on a link so that they could download a software, 
After victims downloading it and executed the exe file, meterpreter will able to listen to the session.

For more information, Please refer to the MSSP Documentation.

##System Information
Language : Python (for automate the attack), Ruby (for communicating with MSFVenom)

Hacking Tools : Metasploit Framework (MSFVenom)

Hosting : Apache2

##Remark (Please Read Carefully!)
- This system is developed for EDUCATION PURPOSE ONLY. Developers will NOT BEAR RESPONSIBILITIES for ANY damage, liabilities, losses! 
- This system was only tested between virtual machines, it might not work in "real-live" environment.
- This system ONLY WORK IN LINUX, Windows will not able to work.

##Instruction
Run python Desktop/MSSP/main.py command in terminal from HOME DIRECTORY 

	example:
	root@kali:~# python Desktop/MSSP/main.py

##Required System Information
###Operating System : 
Linux (Preferable Kali Linux)
###Software : 
Hacking Tools : Metasploit

Hosting : Apache2

Network Connection : Present (In order to send email)

###File Path

In order to excute the system:

- Please ensure Apache2 has installed into your system.

- Execution Directory Must be from HOME Directory

		Instruction: 
		1) cd 
		2) Desktop/MSSP/main.py

- Please ensure all source code in correct Path: 

		 ===========================================
		|   Souce Code	 | 	     	Path			|
		|================|==========================|
		|    main.py	 | Desktop/MSSP/main.py	    |
		|----------------|--------------------------|
		|  listener.rb	 | Desktop/MSSP/listener.rb |
		|----------------|--------------------------|
		|  solution.rb	 | Desktop/MSSP/solution.rb |
		 ===========================================

##System Flow
1) Validation for file path

2) Start Postgresql service

3) Remove any previous virus.exe created by MSFVenom

4) Start Apache server

5) Obtain local IP

6) Generate Payload 
	
	Payload = windows/meterpreter/reverse_tcp
	LHOST = IP Obtained
	LPORT = 444 (prefixed)
	Encoder = x86/shikata_ga_nai
	File Type = .exe
	Output File name = virus.exe
	File Path = /var/www/html/virus.exe (Inside apache folder)

7) Request for victim's email and send via gmail

8) Run listener.rb to create listener via msfconsole with multi/handler and exploit

9) As soon as victim executed the virus.exe, meterpreter will obtain session and open the connection

10) solution.rb will be execute as countermeasurement for exploitation (as part of requirement for assignment)

##About Me
Please refer to http://www.kavierkoo.com for more information.

##License
Copyright © All Rights Reserved. Asia Pacific University