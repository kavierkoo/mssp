def list_exec(session,cmdlst)
    
    r=''
    session.response_timeout=120
    cmdlst.each do |cmd|
       begin
          print_status "Running command #{cmd}"
          r = session.sys.process.execute("cmd.exe /c #{cmd}", nil, {'Hidden' => true, 'Channelized' => true})
          while(d = r.channel.read)
              print_status("#{d}")
          end
          r.channel.close
          r.close
       rescue ::Exception => e
          print_error("Error Running Command #{cmd}: #{e.class} #{e}")
       end
    end
 end

def menu
	print_status("==========================================================")
	print_status("                     Solution to Prevent")
	print_status("==========================================================")
	print_status(" 1. Turn on firewall!")
	print_status(" 2. Check running process!")
	print_status(" 3. Kill the running process!")
	print_status(" 0. Exit Solution to Prevent!")
	print(" Enter option: ")
	input = gets.chomp

	case input
	when "1"	
		firewall_commands = ["netsh advfirewall show all state","netsh advfirewall set allprofiles state on"]
		list_exec(client,firewall_commands)
		
		menu
	when "2"
		process_commands = ["wmic process list brief"]
		list_exec(client,process_commands)
		menu
	when "3"
		print("Enter a task pid to kill: ")
		task = gets.chomp 
		kill_commands = ["Taskkill /PID #{task} /F"]
		list_exec(client,kill_commands)
		menu
	when "0"
		print_status(" Exit!")
		exit_commands = ["Exit"]
		list_exec(client,exit_commands)
		print_status("[*]In order to start Solution to Prevent agian, use command below...")
		print_status("\trun Desktop/MSSP/solution.rb")
	else 
		menu
	end
end

menu
